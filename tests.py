import os
import shutil
from io import BytesIO
from pathlib import Path
from time import sleep
from unittest.mock import patch
from uuid import uuid4

import OrcFxAPI as ofx
import boto3
import pytest
import yaml
from OrcFxAPI import DLLError
from cryptography.fernet import Fernet

from ofxcloudsync.ofx_sync import run_local_sync, s3sync

s3 = boto3.client('s3')


@pytest.fixture()
def temp_bucket(request):
    uuid__hex = uuid4().hex
    s3.create_bucket(Bucket=uuid__hex, CreateBucketConfiguration={'LocationConstraint': "eu-west-2"})

    def clean_up():
        s3r = boto3.resource('s3')
        b = s3r.Bucket(uuid__hex)
        b.objects.all().delete()
        b.delete()

    request.addfinalizer(clean_up)
    return uuid__hex


@pytest.fixture()
def sim_file(request, temp_bucket, tmpdir_factory):
    ex = ofx.Model("example.dat")
    ex.general.tags['BUCKET'] = temp_bucket
    post_calc = Path(__file__).parent.joinpath("ofxcloudsync", "pca_ofx2cloud.py").absolute()
    ex.general.PostCalculationActionScriptFileName = post_calc
    ex.RunSimulation()
    _dir = tmpdir_factory.mktemp("test")
    fn = _dir.join("example.sim")
    ex.SaveSimulation(str(fn))

    def clean_up():
        shutil.rmtree(str(_dir))

    request.addfinalizer(clean_up)

    return fn


@pytest.fixture()
def key_file(request, tmpdir_factory):
    local_path = tmpdir_factory.mktemp("test")
    temp_key_path = local_path.join("temp_secret.key")
    key = Fernet.generate_key()
    with open(temp_key_path, 'wb') as f:
        f.write(key)

    def clean_up():
        shutil.rmtree(str(local_path))

    request.addfinalizer(clean_up)

    return temp_key_path


@pytest.fixture()
def local_root(temp_bucket, tmpdir_factory, key_file):
    temp_root = tmpdir_factory.mktemp("root")
    config = {"bucket": temp_bucket, "sync": ["my_ofx_project", "my_ofx_project1", "my_ofx_project2"],
              "root_folder": str(temp_root),
              'key_file': key_file}
    ini = temp_root.join("sync.ofx")
    with open(ini, 'w') as ymlfile:
        yaml.dump(config, ymlfile)
    return temp_root


def test_post_calc(sim_file, local_root):
    ex = ofx.Model(sim_file)
    with open(str(local_root.join("sync.ofx"))) as lr:
        temp_config = yaml.full_load(lr)
        with patch('ofxcloudsync.ofx_sync.load_sync_ofx') as sync:
            with patch('ofxcloudsync.load_sync_ofx') as pca:
                sync.return_value = temp_config
                pca.return_value = temp_config
                ex.ExecutePostCalculationActions(sim_file, ofx.atInProcPython)
                cloud_bytes = BytesIO()
                s3.download_fileobj(temp_config['bucket'], f"{ex.general.tags.get('FOLDER')}/example.sim", cloud_bytes)
                with pytest.raises(DLLError):
                    cloud_bytes.seek(0)
                    ofx.Model().LoadSimulationMem(cloud_bytes.read())
                run_local_sync()
                project_root = os.path.join(local_root, ex.general.tags['FOLDER'])
                assert "example.sim" in os.listdir(project_root)
                assert "example.dat" in os.listdir(project_root)
                ofx.Model(os.path.join(project_root, "example.sim"))
                pass

def test_sync(temp_bucket, local_root):
    new_folder = local_root.join("new_folder")
    for i in range(1025):
        s3.upload_fileobj(BytesIO(bytes(i)), temp_bucket, f"new_folder/{i}.file")
    for s in range(5):
        s3.upload_fileobj(BytesIO(bytes(s)), temp_bucket, f"new_folder/sub/{s}.file")

    s3sync(temp_bucket, str(local_root), ["new_folder"])
    for i in range(1025):
        assert f"{i}.file" in os.listdir(new_folder)
    for s in range(5):
        assert f"{s}.file" in os.listdir(os.path.join(new_folder, 'sub'))
    s3.upload_fileobj(BytesIO(b'i=5 updated'), temp_bucket, f"new_folder/5.file")

    s3sync(temp_bucket, str(local_root), ["new_folder"])

    with open(new_folder.join("5.file"), 'rb') as rf:
        assert rf.read() == b'i=5 updated'
